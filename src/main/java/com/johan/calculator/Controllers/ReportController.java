package com.johan.calculator.Controllers;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.johan.calculator.Entities.ReportsEntity;
import com.johan.calculator.Services.ReportService;

@RestController
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/reports")

public class ReportController {

	@Autowired
	ReportService reportService;
	
	
	@PostMapping(path = "/save_report",produces = "application/json")
	
	public HashMap<String, String> saveReport (@RequestBody ReportsEntity reportsEntity) {
		return reportService.saveReport(reportsEntity);
	}
	
	
	
	@GetMapping(path = "/query")
	public HashMap<String, Object> getReportsById(
			@RequestParam String id,
			@RequestParam int week
			)
	{
		List<ReportsEntity> responseList = reportService.findAllReport(id, week);
		if(responseList.size() == 0) {
			HashMap<String, Object> response = new HashMap<String, Object>();
			int value = 0;
			response.put("response", value);
			return response;
		} else {
			HashMap<String, Object> response = new HashMap<String, Object>();
			
			int weekHours = 0;
			
			for (int i = 0; i < responseList.size(); i++) {
				ReportsEntity registro = responseList.get(i);
				weekHours = weekHours + registro.getHours();
			}
			
			HashMap<String, Object> allRecors = new HashMap<String, Object>();
			allRecors.put("total_horas", weekHours);
			allRecors.put("registros", responseList);
			
			response.put("response", allRecors);
			return response;
		}
	}
	
	
}
