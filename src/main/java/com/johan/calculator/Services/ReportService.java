package com.johan.calculator.Services;

import java.util.HashMap;
import java.util.List;

import com.johan.calculator.Entities.ReportsEntity;


public interface ReportService {
	public HashMap<String, String> saveReport(ReportsEntity reportsEntity);
	public List<ReportsEntity> findAllReport(String id, int week);
}
