package com.johan.calculator.Services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.time.temporal.WeekFields;

import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import com.johan.calculator.Entities.ReportsEntity;
import com.johan.calculator.Repositories.ReportsRepository;
import com.johan.calculator.Repositories.ReportsRepositoryJPA;

@Service
public class ReportServiceImp implements ReportService {

	@Autowired
	ReportsRepository reportsRepository;
	
	@Autowired
	ReportsRepositoryJPA reportsRepositoryJPA;
	
	
	@Override
	public HashMap<String, String> saveReport(ReportsEntity reportsEntity) {
		// TODO Auto-generated method stub
		if(
				reportsEntity.getService_identification() != null &&
				reportsEntity.getTechnician_identification() != null &&
				reportsEntity.getFinal_date() != null &&
				reportsEntity.getInitial_date() != null
		) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date initialDate = null;
			Date finalDate = null;
			try {
				initialDate = format.parse(reportsEntity.getInitial_date());
				finalDate = format.parse(reportsEntity.getFinal_date());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			System.out.println("fecha inicial: " + initialDate);
			System.out.println("fecha final: " + finalDate);
	        
			if (initialDate.after(finalDate)) {
				HashMap<String, String> response = new HashMap<String, String>();
				response.put("response", "El reporte no se pudo guardar, porque la fecha inicio es mayor que la fecha fin");
				return response;
			}
			
			long diff = finalDate.getTime() - initialDate.getTime();
			long diffHours = diff / (60 * 60 * 1000) % 24;
			
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(initialDate);
			
			reportsEntity.setWeek(calendar.get(Calendar.WEEK_OF_YEAR));
			reportsEntity.setHours((int) diffHours);
			reportsRepository.save(reportsEntity);
			
			
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("response", "registro guardado");
			return response;
		}
		else {
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("response", "Todos los campos deben estar diligenciados");
			return response;
			
		}
	}
	
	
	@Override
	public List<ReportsEntity> findAllReport(String id, int week) {
		return reportsRepositoryJPA.findByIdentification(id, week);
	}

	
	
}
