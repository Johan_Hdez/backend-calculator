package com.johan.calculator.Entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="reports")
public class ReportsEntity {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "report_id", columnDefinition = "VARCHAR(255)")
    private UUID report_id;

    @Column(name = "service_identification", columnDefinition = "VARCHAR(255)")
    private String service_identification;

    @Column(name = "technician_identification", columnDefinition = "VARCHAR(255)")
    private String technician_identification;

    @Column(name = "initial_date", columnDefinition = "DATE")
    private String initial_date;

    @Column(name = "final_date", columnDefinition = "DATE")
    private String final_date;
    
    @Column(name = "hours")
    private int hours;
    
    @Column(name = "week")
    private int week;
    
    

    public ReportsEntity() {
		super();
	}

	

    public ReportsEntity(UUID report_id, String service_identification, String technician_identification,
    		String initial_date, String final_date, int hours, int week) {
		super();
		this.report_id = report_id;
		this.service_identification = service_identification;
		this.technician_identification = technician_identification;
		this.initial_date = initial_date;
		this.final_date = final_date;
		this.hours = hours;
		this.week = week;
	}



	public int getHours() {
		return hours;
	}



	public void setHours(int hours) {
		this.hours = hours;
	}



	public int getWeek() {
		return week;
	}



	public void setWeek(int week) {
		this.week = week;
	}



	public UUID getReport_id() {
        return report_id;
    }

    public void setReport_id(UUID report_id) {
        this.report_id = report_id;
    }

    public String getService_identification() {
        return service_identification;
    }

    public void setService_identification(String service_identification) {
        this.service_identification = service_identification;
    }

    public String getTechnician_identification() {
        return technician_identification;
    }

    public void setTechnician_identification(String technician_identification) {
        this.technician_identification = technician_identification;
    }

    public String getInitial_date() {
        return initial_date;
    }

    public void setInitial_date(String initial_date) {
        this.initial_date = initial_date;
    }

    public String getFinal_date() {
        return final_date;
    }

    public void setFinal_date(String final_date) {
        this.final_date = final_date;
    }
}
