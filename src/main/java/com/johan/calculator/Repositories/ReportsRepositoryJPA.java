package com.johan.calculator.Repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.johan.calculator.Entities.ReportsEntity;


@Repository
public interface ReportsRepositoryJPA extends JpaRepository<ReportsEntity, UUID>{
	
	@Query(
			value = "SELECT * FROM reports u WHERE u.technician_identification = ?1 and u.week =?2", 
			nativeQuery = true)
	List<ReportsEntity> findByIdentification(String id, int week);


}
