package com.johan.calculator.Repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.johan.calculator.Entities.ReportsEntity;

@Repository
public interface ReportsRepository extends CrudRepository<ReportsEntity, UUID>{
}
